/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mau-Mau
  Copyright (c) 2017 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <stdexcept>
#include <boost/asio.hpp>

#include "tcp-server.hh"

int main()
{
  try
  {
    boost::asio::io_service io_service;
    TcpServer server (io_service);
    io_service.run ();
  }
  catch (std::exception& e)
  {
    std::cerr << e.what () << std::endl;
  }

  return 0;
  
  /*
  auto deck = new_deck ();
  shuffle_deck (deck);
  print_deck (deck);
  return 0;
  */
}
