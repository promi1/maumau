/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mau-Mau
  Copyright (c) 2017 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <string>
#include <map>
#include <vector>

enum class Suite
  {
    Diamonds,
    Hearts,
    Spades,
    Clubs
  };

enum class Rank
  {
    Seven,
    Eight,
    Nine,
    Jack,
    Queen,
    King,
    Ten,
    Ace
  };

extern std::map<Suite, std::string> suite_names;
std::string to_string (const Suite suite);

extern std::map<Rank, std::string> rank_names;
std::string to_string (const Rank rank);

struct Card
{
  Suite suite;
  Rank rank;
  Card (Suite suite, Rank rank);
};

std::vector<Card> new_deck ();
void print_deck (const std::vector<Card> &deck);
void shuffle_deck (std::vector<Card> &deck);
