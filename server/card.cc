/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mau-Mau
  Copyright (c) 2017 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "card.hh"

#include <iostream>
#include <random>
#include <algorithm>

std::map<Suite, std::string> suite_names
{
  { Suite::Diamonds, "Diamonds" },
    { Suite::Hearts, "Hearts"  },
      { Suite::Spades, "Spades" },
	{ Suite::Clubs, "Clubs" }
};

std::string to_string (const Suite suite)
{
  return suite_names.at (suite);
}

std::map<Rank, std::string> rank_names
{
  { Rank::Seven, "7" },
    { Rank::Eight, "8" },
      { Rank::Nine, "9" },
	{ Rank::Jack, "Jack" },
	  { Rank::Queen, "Queen" },
	    { Rank::King, "King" },
	      { Rank::Ten, "10" },
		{ Rank::Ace, "Ace" }
};

std::string to_string (const Rank rank)
{
  return rank_names.at (rank);
}

Card::Card (Suite suite, Rank rank) : suite (suite), rank (rank)
  {
  }

std::vector<Card> new_deck ()
{
  std::vector<Card> v;
  for (auto suite = static_cast<int>(Suite::Diamonds); suite <= static_cast<int>(Suite::Clubs); suite++)
    {
      for (auto rank = static_cast<int>(Rank::Seven); rank <= static_cast<int>(Rank::Ace); rank++)
	{
	  v.emplace_back (static_cast<Suite>(suite), static_cast<Rank>(rank));
	}
    }
  return v;
}

void print_deck (const std::vector<Card> &deck)
{
  std::cout << "Deck:\n\n";
  for (const auto &card : deck)
    {
      std::cout << to_string (card.suite) << " " << to_string (card.rank) << "\n";
    }
}

void shuffle_deck (std::vector<Card> &deck)
{
  std::random_device rd;
  std::mt19937 g(rd());
 
  std::shuffle(deck.begin(), deck.end(), g);
}
