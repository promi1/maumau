/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mau-Mau
  Copyright (c) 2017 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "tcp-server.hh"

using boost::asio::ip::tcp;

TcpServer::TcpServer (boost::asio::io_service& io_service)
  : m_acceptor (io_service, tcp::endpoint (tcp::v4 (), 60110))
{
  start_accept ();
}

void TcpServer::start_accept ()
{
  auto connection = TcpConnection::create (m_acceptor.get_io_service (), m_game);
  auto handle_accept_lambda =
    [this, connection] (const boost::system::error_code& error)
    {
      handle_accept (connection, error);
    };
  m_acceptor.async_accept (connection->socket(), handle_accept_lambda);
}

void TcpServer::handle_accept(TcpConnection::pointer connection,
			      const boost::system::error_code& error)
{
  if (!error)
  {
    connection->start();
  }

  start_accept();
}
