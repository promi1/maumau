/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mau-Mau
  Copyright (c) 2017,2018 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "tcp-connection.hh"

#include <iostream>

TcpConnection::pointer TcpConnection::create (boost::asio::io_service& io_service,
                                              Game &game)
{
  return pointer (new TcpConnection (io_service, game));
}

boost::asio::ip::tcp::socket& TcpConnection::socket ()
{
  return m_socket;
}

void TcpConnection::start ()
{
  do_read_message ();
}

void TcpConnection::authenticate (const std::string &name)
{
  auto *proto = m_proto.get ();
  std::cout << "authentication: " << name << "\n";
  try
    {
      m_player_number = m_game.add (name);
      if (!maumau_proto_send_accept (proto, m_player_number, name.data ()))
        {
          std::cerr << "sending accept failed\n";
          m_socket.close ();
        }
    }
  catch (const std::runtime_error &e)
    {
      if (!maumau_proto_send_reject (proto, e.what ()))
        {
          std::cerr << "sending reject failed\n";
        }
      m_socket.close ();
    }
};

void TcpConnection::text_message (const MaumauTextMessage &text_message)
{
  auto *proto = m_proto.get ();
  std::cout << "text message\n";
  maumau_proto_send_text_message (proto, text_message.player_number,
                                  text_message.format, text_message.message);
}

void TcpConnection::authenticate_static (const char *name, void *user_data)
{
  auto *self = static_cast<TcpConnection*> (user_data);
  self->authenticate (std::string (name));
}

void TcpConnection::text_message_static (const MaumauTextMessage *text_message,
                                         void *user_data)
{
  auto *self = static_cast<TcpConnection*> (user_data);
  self->text_message (*text_message);
}

void TcpConnection::process_message ()
{
  auto proto = m_proto.get ();
  if (maumau_proto_consume_read_data (proto, m_read_buffer.data (), m_read_buffer.size ()))
    {
      if (maumau_proto_has_message (proto))
        {
          std::cout << "got message from client\n";
          auto message_type = maumau_proto_get_message_type (proto);
          if (message_type == MAUMAU_MESSAGE_TYPE_AUTHENTICATION)
            {
              maumau_proto_recv_authentication (proto, authenticate_static, this);
            }
          else if (message_type == MAUMAU_MESSAGE_TYPE_TEXT_MESSAGE)
            {
              maumau_proto_recv_text_message (proto, text_message_static, this);
            }
        }
    }
  do_read_message ();
}

void TcpConnection::do_read_message ()
{
  auto self (shared_from_this ());
  m_read_buffer.resize (maumau_proto_get_next_read_size (m_proto.get ()));
  auto read_func = [this, self] (boost::system::error_code ec, std::size_t length)
    {
      (void) length;
      if (ec)
        {
          if ((ec == boost::asio::error::eof) ||
              (ec == boost::asio::error::connection_reset ))
            {
              std::cerr << "client disconnected\n";
            }
          else
            {
              std::cerr << "unknown read error while reading a message from a client\n";
            }
          return;
        }
      process_message ();
    };
  boost::asio::async_read (m_socket,
                           boost::asio::buffer (m_read_buffer.data (), m_read_buffer.size ()),
                           read_func);
}

MaumauProto_t* make_proto (void *self)
{
  auto write_func = [] (const unsigned char *data, int data_length,
                        void *user_data)
    {
      auto self = static_cast<TcpConnection*> (user_data);
      try
        {
          self->write (data, data_length);
          return (maumau_bool) MAUMAU_TRUE;
        }
      catch (const std::runtime_error &e)
        {
          std::cerr << "write error " << e.what () << "\n";
          return (maumau_bool) MAUMAU_FALSE;
        }
    };
  return maumau_proto_new (write_func, self, [] (void *self) { });
}

void TcpConnection::write (const unsigned char *data, int data_length)
{
  boost::asio::write (m_socket, boost::asio::buffer(data, data_length));
}

TcpConnection::TcpConnection (boost::asio::io_service& io_service, Game &game)
  : m_socket (io_service),
    m_proto (decltype (m_proto) (make_proto (this), &maumau_proto_free)),
    m_game (game)
{
}
