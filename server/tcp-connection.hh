/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mau-Mau
  Copyright (c) 2017,2018 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <string>
#include <boost/asio.hpp>
#include "maumau-proto/maumau-proto.h"
#include "game.hh"

class TcpConnection
  : public std::enable_shared_from_this <TcpConnection>
{
public:
  typedef std::shared_ptr<TcpConnection> pointer;
  static pointer create (boost::asio::io_service& io_service, Game &game);
  boost::asio::ip::tcp::socket& socket ();
  void start ();
  void write (const unsigned char *data, int data_length);
private:
  TcpConnection (boost::asio::io_service& io_service, Game &game);
  void process_message ();
  void do_read_message ();
  void authenticate (const std::string &name);
  void text_message (const MaumauTextMessage &text_message);
  static void authenticate_static (const char *name, void *user_data);
  static void text_message_static (const MaumauTextMessage *text_message,
                                   void *user_data);
  boost::asio::ip::tcp::socket m_socket;
  std::unique_ptr<MaumauProto, decltype (&maumau_proto_free)> m_proto;
  std::vector<char> m_read_buffer;
  Game &m_game;
  int m_player_number;
};
