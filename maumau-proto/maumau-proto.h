/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mau-Mau
  Copyright (c) 2017,2018 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

  typedef int maumau_bool;
#define MAUMAU_FALSE 0
#define MAUMAU_TRUE !MAUMAU_FALSE

  typedef enum MaumauMessageType_t
    {
      MAUMAU_MESSAGE_TYPE_INVALID = 0,
      MAUMAU_MESSAGE_TYPE_AUTHENTICATION = 1,
      MAUMAU_MESSAGE_TYPE_REJECT = 2,
      MAUMAU_MESSAGE_TYPE_ACCEPT = 3,
      MAUMAU_MESSAGE_TYPE_TEXT_MESSAGE = 4
    } MaumauMessageType;

  typedef enum MaumauTextMessageFormat_t
    {
      MAUMAU_TEXT_MESSAGE_FORMAT_PLAIN = 0
    } MaumauTextMessageFormat;
  
  typedef struct MaumauTextMessage_t
  {
    const uint32_t *player_number;
    MaumauTextMessageFormat format;
    const char *message;
  } MaumauTextMessage;

  //MaumauTextMessage* maumau_text_message_new ();
  //void maumau_text_message_free (MaumauTextMessage *self);
  
  typedef struct MaumauPlayer_t
  {
    uint32_t number;
    const char *name;
  } MaumauPlayer;
  
  typedef maumau_bool (*MaumauWriteDataFunc) (const uint8_t *data,
                                              int data_length,
                                              void *user_data);
  typedef void (*MaumauUserDataFreeFunc) (void *user_data);
  typedef void (*MaumauAuthenticationFunc) (const char *name, void *user_data);
  typedef void (*MaumauRejectFunc) (const char *reason, void *user_data);
  typedef void (*MaumauAcceptFunc) (const MaumauPlayer *player,
                                    void *user_data);
  typedef void (*MaumauTextMessageFunc) (const MaumauTextMessage *msg,
                                         void *user_data);
  typedef struct MaumauProto_t MaumauProto;

  MaumauProto* maumau_proto_new (const MaumauWriteDataFunc write_data_func,
                                 void* user_data,
                                 const MaumauUserDataFreeFunc user_data_free_func);
  void maumau_proto_free (MaumauProto *self);

  uint16_t maumau_proto_get_next_read_size (MaumauProto *self);
  maumau_bool maumau_proto_consume_read_data (MaumauProto *self,
                                              const char *data,
                                              const size_t data_length);
  maumau_bool maumau_proto_has_message (MaumauProto *self);
  MaumauMessageType maumau_proto_get_message_type (MaumauProto *self);

  void maumau_proto_recv_authentication (MaumauProto *self,
                                         MaumauAuthenticationFunc func,
                                         void *user_data);
  maumau_bool maumau_proto_send_authentication (MaumauProto *self,
                                                const char *name);
  void maumau_proto_recv_accept (MaumauProto *self,
                                 MaumauAcceptFunc func,
                                 void *user_data);
  maumau_bool maumau_proto_send_accept (MaumauProto *self,
                                        const unsigned int player_number,
                                        const char *player_name);
  void maumau_proto_recv_reject (MaumauProto *self,
                                 MaumauRejectFunc func,
                                 void *user_data);
  maumau_bool maumau_proto_send_reject (MaumauProto *self, const char *reason);
  void maumau_proto_recv_text_message (MaumauProto *self,
                                       MaumauTextMessageFunc func,
                                       void *user_data);
  maumau_bool maumau_proto_send_text_message (MaumauProto *self,
                                              const uint32_t *player_number,
                                              const MaumauTextMessageFormat format,
                                              const char *message);

#ifdef __cplusplus
}
#endif
