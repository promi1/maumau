/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mau-Mau
  Copyright (c) 2017,2018 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "maumau-proto.h"
#include "endian-utils.hh"
#include "messages.hh"

#include <memory>
#include <sstream>
#include <iostream>

#include <google/protobuf/message.h>

const uint16_t prefix_size = 6;
  
struct MaumauProto_t
{
  const MaumauWriteDataFunc write_data_func;
  void* user_data;
  const MaumauUserDataFreeFunc user_data_free_func;
  uint16_t next_read_size = prefix_size;
  bool next_is_prefix = true;
  MaumauMessageType type = MAUMAU_MESSAGE_TYPE_INVALID;
  std::unique_ptr<google::protobuf::Message> message;

  MaumauProto_t (const MaumauWriteDataFunc write_data_func,
                 void* user_data,
                 const MaumauUserDataFreeFunc user_data_free_func)
    : write_data_func (write_data_func), user_data (user_data),
      user_data_free_func (user_data_free_func)
  {
  }
  
  ~MaumauProto_t ()
  {
    user_data_free_func (user_data);
  }

  void write_data (const std::vector<uint8_t> &data)
  {
    write_data_func (data.data (), data.size (), user_data);
  }

  static std::vector<uint8_t> make_prefix (uint16_t type, uint32_t len)
  {
    std::vector<uint8_t> prefix;
    EndianUtils::add_to_u16be (prefix, type);
    EndianUtils::add_to_u32be (prefix, len);
    return prefix;
  }

  void send_data (uint16_t type, const std::vector<uint8_t> &data)
  {
    uint32_t len = data.size ();
    auto prefix = make_prefix (type, len);
    write_data (prefix);
    write_data (data);
  }

  void send_message (uint16_t type, const ::google::protobuf::Message &msg)
  {
    std::stringstream os;
    msg.SerializeToOstream (&os);
    std::string os_str = os.str ();
    send_data (type, std::vector<uint8_t> (std::begin (os_str),
                                                 std::end (os_str)));
  }

  void send (int type, const ::google::protobuf::Message& msg)
  {
    send_message (type, msg);
  }

};

template <typename T>
inline void maumau_private_send (MaumauProto *self, const T &msg)
{
  self->send (Messages::sym_to_type.at (std::type_index (typeid (T))), msg);
}

extern "C"
{
  MaumauProto*
  maumau_proto_new (const MaumauWriteDataFunc write_data_func,
                    void* user_data,
                    const MaumauUserDataFreeFunc user_data_free_func)
  {
    return new MaumauProto (write_data_func, user_data, user_data_free_func);
  }

  void
  maumau_proto_free (MaumauProto *self)
  {
    delete self;
  }

  uint16_t
  maumau_proto_get_next_read_size (MaumauProto *self)
  {
    return self->next_read_size;
  }
  
  maumau_bool
  maumau_proto_consume_read_data (MaumauProto *self,
                                  const char *data,
                                  const size_t data_length)
  {
    try
      {
        std::vector<uint8_t> v (data, data + data_length);
        if (self->next_is_prefix)
          {
            self->next_is_prefix = false;
            self->next_read_size = EndianUtils::value_from_u32be (v, 2);

            self->type = static_cast<MaumauMessageType>
              (EndianUtils::value_from_u16be (v, 0));
            // Clear last message
            self->message = {};
          }
        else
          {
            self->next_is_prefix = true;
            self->next_read_size = prefix_size;

            // Type must stay intact!
            //self->type = MAUMAU_MESSAGE_TYPE_INVALID;
            self->message = Messages::msg_from_type (self->type);
            std::stringstream is (std::string (std::begin (v), std::end (v)));
            self->message->ParseFromIstream (&is);
          }
        return MAUMAU_TRUE;
      }
    catch (const std::runtime_error &e)
      {
        return MAUMAU_FALSE;
      }
  }
  
  maumau_bool
  maumau_proto_has_message (MaumauProto *self)
  {
    return (self->message == nullptr) ? MAUMAU_FALSE: MAUMAU_TRUE;
  }
  
  MaumauMessageType
  maumau_proto_get_message_type (MaumauProto *self)
  {
    return self->type;
  }

  void
  maumau_proto_recv_authentication (MaumauProto *self,
                                    MaumauAuthenticationFunc func,
                                    void *user_data)
  {
    auto &msg = dynamic_cast<MauMau::Authentication&> (*self->message);
    func (msg.name ().data (), user_data);
  }
  
  maumau_bool
  maumau_proto_send_authentication (MaumauProto *self, const char *name)
  {
    try
      {
        MauMau::Authentication msg;
        msg.set_name (name);
        maumau_private_send (self, msg);
        return MAUMAU_TRUE;
      }
    catch (const std::runtime_error &e)
      {
        return MAUMAU_FALSE;
      }
  }

  void
  maumau_proto_recv_accept (MaumauProto *self, MaumauAcceptFunc func,
                            void *user_data)
  {
    auto &msg = dynamic_cast<MauMau::Accept&> (*self->message);
    const auto &p = msg.player ();
    MaumauPlayer player;
    player.number = p.number ();
    player.name = p.name ().data ();
    func (&player, user_data);
  }
  
  maumau_bool
  maumau_proto_send_accept (MaumauProto *self, const unsigned int player_number,
                            const char *player_name)
  {
    try
      {
        MauMau::Accept msg;
        auto *player = msg.mutable_player ();
        player->set_name (player_name);
        player->set_number (player_number);
        maumau_private_send (self, msg);
        return MAUMAU_TRUE;
      }
    catch (const std::runtime_error &e)
      {
        return MAUMAU_FALSE;
      }
  }
  
  void
  maumau_proto_recv_reject (MaumauProto *self, MaumauRejectFunc func,
                            void *user_data)
  {
    auto &msg = dynamic_cast<MauMau::Reject&> (*self->message);
    func (msg.reason ().data (), user_data);
  }
  
  maumau_bool
  maumau_proto_send_reject (MaumauProto *self, const char *reason)
  {
    try
      {
        MauMau::Reject msg;
        msg.set_reason (reason);
        maumau_private_send (self, msg);
        return MAUMAU_TRUE;
      }
    catch (const std::runtime_error &e)
      {
        return MAUMAU_FALSE;
      }
  }

  void maumau_proto_recv_text_message (MaumauProto *self,
                                       MaumauTextMessageFunc func,
                                       void *user_data)
  {
    auto &msg = dynamic_cast<MauMau::TextMessage&> (*self->message);
    MaumauTextMessage text_message;
    uint32_t player_number;
    if (msg.has_player_number ())
      {
        player_number = msg.player_number ();
        text_message.player_number = &player_number;
      }
    else
      {
        text_message.player_number = nullptr;
      }
    text_message.format = static_cast<MaumauTextMessageFormat> (msg.format ());
    text_message.message = msg.message ().data ();
    func (&text_message, user_data);
  }
  
  maumau_bool maumau_proto_send_text_message (MaumauProto *self,
                                              const uint32_t *player_number,
                                              const MaumauTextMessageFormat format,
                                              const char *message)
  {
    try
      {
        MauMau::TextMessage msg;
        if (player_number != nullptr)
          {
            msg.set_player_number (*player_number);
          }
        else
          {
            msg.clear_player_number ();
          }
        msg.set_format (static_cast<MauMau::TextMessage_Format> (format));
        msg.set_message (message);
        maumau_private_send (self, msg);
        return MAUMAU_TRUE;
      }
    catch (const std::runtime_error &e)
      {
        return MAUMAU_FALSE;
      }
  }
}
