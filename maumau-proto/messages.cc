/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mau-Mau
  Copyright (c) 2017,2018 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "messages.hh"

std::unordered_map<std::type_index, uint16_t> Messages::sym_to_type
{
  {std::type_index(typeid (MauMau::Authentication)), 1},
  {std::type_index(typeid (MauMau::Reject)), 2},
  {std::type_index(typeid (MauMau::Accept)), 3},
  {std::type_index(typeid (MauMau::TextMessage)), 4}
};

std::unique_ptr<::google::protobuf::Message> Messages::msg_from_type (uint16_t type)
{
  switch (type)
    {
    case 1:
      return std::make_unique<MauMau::Authentication> ();
    case 2:
      return std::make_unique<MauMau::Reject> ();
    case 3:
      return std::make_unique<MauMau::Accept> ();
    case 4:
      return std::make_unique<MauMau::TextMessage> ();
    default:
      throw std::runtime_error ("Invalid message type");
    }
}
