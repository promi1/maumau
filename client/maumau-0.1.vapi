/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mau-Mau
  Copyright (c) 2017,2018 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
[CCode (cheader_filename = "maumau-proto/maumau-proto.h")]
namespace Maumau
{
  public enum MessageType {
    INVALID,
    AUTHENTICATE,
    REJECT,
    ACCEPT,
    TEXT_MESSAGE
  }

  [Compact]
  public class TextMessage {
    public enum Format {
      PLAIN
    }
    public TextMessage ();
    public uint32? player_number;
    public Format format;
    public string message;
  }

  [Compact]
  public class Player {
    public uint32 number;
    public string name;
  }

  public delegate bool WriteDataFunc (uint8[] data);

  public delegate void AuthenticationFunc (string name);
  public delegate void RejectFunc (string reason);
  public delegate void AcceptFunc (Player player);
  public delegate void TextMessageFunc (TextMessage text_message);
  
  [Compact]
  public class Proto {
    public Proto (owned WriteDataFunc func);

    public uint get_next_read_size ();
    public bool consume_read_data (uint8[] data);
    public bool has_message ();
    public MessageType get_message_type ();

    public void recv_authentication (AuthenticationFunc func);
    public bool send_authentication (string name);
    public void recv_accept (AcceptFunc func);
    public bool send_accept (Player player);
    public void recv_reject (RejectFunc func);
    public bool send_reject (string reason);
    public void recv_text_message (TextMessageFunc func);
    public bool send_text_message (uint32? player_number,
                                   TextMessage.Format format,
                                   string message);
  }
}
