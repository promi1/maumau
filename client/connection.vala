/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mau-Mau
  Copyright (c) 2017,2018 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
public class Connection {
  private Maumau.Proto proto;
  private SocketConnection socket_connection;
  private InputStream socket_input;
  private OutputStream socket_output;
  private SocketClient socket_client;
  private string hostname;
  private uint16 port;
  private string name;

  public Connection (string hostname, uint16 port, string name) {
    this.hostname = hostname;
    this.port = port;
    this.name = name;
    this.socket_client = new SocketClient ();
    // socket_client.tls = true;
    // socket_client.event.connect (socket_client_event);
  }

  private static void close_stream_noexcept (IOStream stream) {
    try {
      stream.close ();
    }
    catch (Error e) {
      if (!(e is IOError.CLOSED)) {
        stderr.printf (@"error: could not close stream: $(e.message)\n");
      }
    }
  }
    
  public void connect () {
    var resolver = Resolver.get_default ();
    List<InetAddress> addresses;
    try {
      addresses = resolver.lookup_by_name (hostname);
    }
    catch (Error e) {
      stderr.printf (@"error: could not lookup server addresses: $(e.message)\n");
      return;
    }
    foreach (var address in addresses) {
      var inet_address = new InetSocketAddress (address, port);
      try {
        socket_connection = socket_client.connect (inet_address);
        // connection_state = ConnectionState.CONNECTED;
        socket_output = socket_connection.output_stream;
        socket_input = socket_connection.input_stream;
        proto = new Maumau.Proto ((data) => {
            try {
              size_t bytes_written;
              socket_output.write_all (data, out bytes_written);
              return true;
            }
            catch (IOError e) {
              stderr.printf (@"error: could not write data to socket: $(e.message)\n");
              return false;
            }
          });
        proto.send_authentication (name);
        receive.begin ();
        /*
        Timeout.add_seconds (15, () => {
            if (yami_client.send_ping ()) {
              return true;
            }
            else {
              stderr.printf (@"error: could not send ping to socket");
              close_stream_noexcept (socket_connection);
              return false;
            }
          });
        */
        break;
      }
      catch (Error e) {
        stderr.printf (@"error: could not connect: $(e.message)\n");
      }
    }
    
  }

  private async void receive () {
    while (!socket_connection.is_closed ()) {
      try {
        var size = proto.get_next_read_size ();
        // stdout.printf ("size: %u\n", size);
        var bytes = yield socket_input.read_bytes_async (size);
        if (bytes.length == 0) {
          stdout.printf ("the connection was closed by the server\n");
          close_stream_noexcept (socket_connection);
          break;
        }
        if (!proto.consume_read_data (Bytes.unref_to_data (bytes))) {
          stderr.printf ("error: consuming data failed\n");
          close_stream_noexcept (socket_connection);
          break;
        }

        if (proto.has_message ()) {
          var message_type = proto.get_message_type ();
          if (message_type == Maumau.MessageType.ACCEPT)  {
            proto.recv_accept ((player) => {
                stdout.printf ("accept\n");
                stdout.printf ("player.number = %u\n", player.number);
                stdout.printf ("player.name = %s\n", player.name);
                stdout.flush ();
              });
          }
          else if (message_type == Maumau.MessageType.REJECT) {
            proto.recv_reject ((msg) => {
                stdout.printf ("reject\n");
                stdout.printf ("msg = %s\n", msg);
                stdout.flush ();
              });
          }
          else if (message_type == Maumau.MessageType.TEXT_MESSAGE) {
            proto.recv_text_message ((msg) => {
                text_message (msg.message);
              });
          }
          else {
            stderr.printf ("error: unknown message type %u\n", message_type);
            close_stream_noexcept (socket_connection);
            break;
          }
        }
      }
      catch (Error e) {
        if (!(e is IOError.CLOSED)) {
          stderr.printf (@"error: could not read data from socket: $(e.message)\n");
          close_stream_noexcept (socket_connection);
          break;
        }
      }
    }
  }

  public signal void text_message (string text);

  public void send_message (string text) {
    proto.send_text_message (null, Maumau.TextMessage.Format.PLAIN, text);
  }
}
