/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mau-Mau
  Copyright (c) 2017,2018 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
public class ApplicationWindow : Gtk.ApplicationWindow {
  private Connection connection;

  private Gtk.Box box;
  private Gtk.Button connect_button;
  private Gtk.Box send_message_box;
  private Gtk.Label send_message_label;
  private Gtk.Entry send_message_entry;
  private Gtk.Button send_message_button;
  private Gtk.TextView messages_text_view;
  private Gtk.Box player_name_box;
  private Gtk.Label player_name_label;
  private Gtk.Entry player_name_entry;
  
  public ApplicationWindow (Application application) {
    this.application = application;

    this.box = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
    this.player_name_box = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
    this.player_name_label = new Gtk.Label ("Player name");
    this.player_name_box.pack_start (this.player_name_label);
    this.player_name_entry = new Gtk.Entry ();
    this.player_name_box.pack_start (this.player_name_entry);
    this.connect_button = new Gtk.Button.with_label ("Connect");
    this.player_name_box.pack_start (this.connect_button);
    this.box.pack_start (this.player_name_box);
    this.send_message_box = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
    this.send_message_label = new Gtk.Label ("Enter text message");
    this.send_message_entry = new Gtk.Entry ();
    this.send_message_button = new Gtk.Button.with_label ("Send");
    this.send_message_box.pack_start (this.send_message_label);
    this.send_message_box.pack_start (this.send_message_entry);
    this.send_message_box.pack_start (this.send_message_button);
    this.box.pack_start (this.send_message_box);
    this.messages_text_view = new Gtk.TextView ();
    this.box.pack_start (this.messages_text_view);
    this.add (box);

    this.connect_button.clicked.connect (() => {
        this.connection = new Connection ("127.0.0.1", 60110, player_name_entry.text);
        this.connection.text_message.connect ((message) => {
            var buffer = this.messages_text_view.buffer;
            Gtk.TextIter it;
            buffer.get_end_iter (out it);
            buffer.insert (ref it, message + "\n", -1);
          });
        this.connection.connect ();
      });

    this.send_message_button.clicked.connect (() => {
        this.connection.send_message (this.send_message_entry.text);
      });

    this.player_name_entry.text = "Player " + Random.int_range (1, 10000).to_string ();
  }
}
